package com.renovo.backend.starter.controller;

import com.renovo.backend.starter.dto.HelloDto;
import com.renovo.backend.starter.service.StarterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping(value = "v1/", produces = MediaType.APPLICATION_JSON_VALUE)
public class StarterController {

    @Autowired
    private StarterService starterService;

    @GetMapping("hello-world")
    public HelloDto helloWorld(){
        return starterService.getHelloWorld();
    }
}
