package com.renovo.backend.starter.service;

import com.renovo.backend.starter.dto.HelloDto;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
public class StarterService {

    public StarterService(){}

    public HelloDto getHelloWorld() {
        return HelloDto.builder()
                .message("Hello World")
                .timestamp(Instant.now())
                .build();
    }
}
