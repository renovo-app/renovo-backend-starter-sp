package com.renovo.backend.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class RenovoBackendStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(RenovoBackendStarterApplication.class, args);
	}

}
