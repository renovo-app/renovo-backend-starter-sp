FROM openjdk:11

COPY target/renovo-backend-starter-sp-*.jar /renovo-backend-starter-sp.jar

EXPOSE 8023

CMD ["java", "-jar", "/renovo-backend-starter-sp.jar"]

